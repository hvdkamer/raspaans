# Maintainer:  Henk van de Kamer <henk@REMOVE.armorica.tk>
# Contributor: Sébastien "Seblu" Luttringer
#              Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
#              Allan McRae <allan@archlinux.org>
#              judd <jvinet@zeroflux.org>

pkgname=coreutils
pkgver=8.32
pkgrel=2
pkgdesc='The basic file, shell and text manipulation utilities of the GNU operating system'
arch=('aarch64')
license=('GPL3')
url='https://www.gnu.org/software/coreutils/'
depends=('glibc' 'acl' 'attr' 'gmp' 'libcap' 'openssl')
source=("https://ftp.gnu.org/gnu/$pkgname/$pkgname-$pkgver.tar.xz"{,.sig}
        fix-ls-aarch64.patch::https://git.savannah.gnu.org/cgit/coreutils.git/patch/?id=10fcb97bd728f09d4a027eddf8ad2900f0819b0a)
validpgpkeys=('6C37DC12121A5006BC1DB804DF6FD971306037D9') # Pádraig Brady
sha256sums=('4458d8de7849df44ccab15e16b1548b285224dbba5f08fac070c1c0e0bcc4cfa'
            'SKIP'
            'de6e425a865630f8d385598e223251a313f3bd6940b51c97aa189228e1b6822e')

prepare() {
  cd $pkgname-$pkgver
  local filename
  for filename in ${source[@]}; do
    if [[ "$filename" =~ \.patch ]]; then
      msg2 "Applying patch ${filename%::*}"
      patch -Np 1 -i ../${filename%::*}
    fi
  done
}

build() {
  cd $pkgname-$pkgver
  ./configure \
      --prefix=/usr \
      --libexecdir=/usr/lib \
      --with-openssl \
      --enable-install-program=hostname \
      --enable-no-install-program=groups,kill,uptime
  make
}

check() {
  cd $pkgname-$pkgver
  make check
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
}
