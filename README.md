# Raspberry Pi Arch Linux ARM no-systemd

As most people know the I in IoT stands for connected to the internet. This means that the device must at any given time always be up-to-date with the latest security patches. Something which is often neglected for various reasons. This gives IoT a bad reputation...

This project was born to show that the above requirement is not that hard to solve. Our IoT device is based on the Raspberry Pi 3 model B+ with a life expectancy of five plus years. This means at least one distribution upgrade and those are hard if physical access is time consuming. Therefor we decided to use a rolling release distribution.

Because we are responsible for fixing every problem an upgrade creates, another requirement is knowing how all components work together. Systemd doesn't do that: it is a complex system which like a virus takes over the complete system. The solution is going back to a normal init process which has done a good job for decades!

For the rolling release distribution [Arch Linux][1] was chosen. It has an easy to understand packaging system. However the main project is only available for the x86_64 processors. It also favors systemd to the point that no packages for alternative init systems are available. The processor problem is solved with the [Arch Linux ARM][2] sister project. And the [Artix Linux][3] fork has multiple choices for init systems although again only for the x86_64 processors.

In this project we start with an Arch Linux ARM installation on the Raspberry Pi. Then we create an OpenRC and netifrc dependency package. With those and a small hack we switch to OpenRC, which is maintained by the [Gentoo][4] developers. After that we exchange all the Arch Linux ARM packages with those from Artix Linux. In essence we create Artix Linux ARM. However as a wink to Raspbian we call it **Rasp**berry Pi **A**rch Linux **A**RM **n**o-**s**ystemd or raspaans for short.

Raspaans is very similar to the Dutch word _[spartaans][5]_. It means very basic and exactly that is our goal.

[1]: https://archlinux.org/
[2]: https://archlinuxarm.org/
[3]: https://artixlinux.org/
[4]: https://www.gentoo.org/
[5]: https://nl.wiktionary.org/wiki/spartaans
